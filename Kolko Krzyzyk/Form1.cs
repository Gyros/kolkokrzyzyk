﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kolko_Krzyzyk
{
    public partial class Form1 : Form
    {
        
        private PictureBox[] boxes;
        char[] board = new char[9];
        char tour = 'o';
        private int counter;
        int ai;
        List<Panel> listPanel = new List<Panel>();
        int index;
        int win;

        void check(int x)
        {
            
            if (
                    (board[0] == board[1] && board[1] == board[2] && board[0] != 'n') ||
                    (board[3] == board[4] && board[4] == board[5] && board[3] != 'n') ||
                    (board[6] == board[7] && board[7] == board[8] && board[6] != 'n') ||
                    (board[0] == board[3] && board[3] == board[6] && board[0] != 'n') ||
                    (board[1] == board[4] && board[4] == board[7] && board[1] != 'n') ||
                    (board[2] == board[5] && board[5] == board[8] && board[2] != 'n') ||
                    (board[0] == board[4] && board[4] == board[8] && board[0] != 'n') ||
                    (board[2] == board[4] && board[4] == board[6] && board[2] != 'n')
                )
            {
                
                string msg = "";
                if(tour == 'o')
                {
                     msg = "Wygrywa krzyżyk!";
                }
                if (tour == 'k')
                {
                    msg = "Wygrywa kółko!";
                }
                MessageBox.Show(msg);
                win = 1;
                clear();
            }
            
            else if (counter == 0)
            {
                MessageBox.Show("Remis");

                win = 1;
                clear();
            }
        }

        private void move(int x)
        {
            
            if (board[x] == 'n')
            {
                --counter;

                if (tour == 'o')
                {
                    boxes[x].Image = Image.FromFile("img/o.bmp");
                    board[x] = 'o';
                    tour = 'k';
                    pictureBox10.Image = Image.FromFile("img/x.bmp");
                }
                else
                {
                    boxes[x].Image = Image.FromFile("img/x.bmp");
                    board[x] = 'k';
                    tour = 'o';
                    pictureBox10.Image = Image.FromFile("img/o.bmp");
                }
                boxes[x].Cursor = Cursors.Default;
                check(x);
                if (ai == 1 && win == 0)
                {
                    Random randd = new Random();
                    int random = randd.Next(0, 9);
                    while(board[random] != 'n')
                    {
                        random = randd.Next(0, 9);
                    }
                    boxes[random].Image = Image.FromFile("img/x.bmp");
                    board[random] = 'k';
                    tour = 'o';
                    pictureBox10.Image = Image.FromFile("img/o.bmp");
                    boxes[random].Cursor = Cursors.Default;
                    check(random);
                    ai = 1;
                }
            }
        }

        void clear()
        {
            win = 0;
            if (ai == 0)
            {
                Random rand = new Random();
                if (rand.Next(0, 2) == 0)
                {
                    tour = 'o';
                }
                else
                    tour = 'k';
            }
            else
                tour = 'o';
            counter = 9;
            for(int j = 0; j<9; j++)
            {
                boxes[j].Image = null;
                boxes[j].Cursor = Cursors.Hand;
            }
            for (int i = 0; i < 9; i++)
            {
                board[i] = 'n';
            }

            pictureBox10.Image = Image.FromFile(tour == 'o' ? "img/o.bmp" : "img/x.bmp");
        }
        public Form1()
        {

            InitializeComponent();



        }

        

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            move(8);
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            move(7);
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            move(6);
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            move(5);
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            move(4);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            move(3);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            move(2);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            move(1);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            move(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clear();
            Menu.Show();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ai = 0;
            listPanel[1].BringToFront();
            Menu.Hide();
            PictureBox[] box = { pictureBox1, pictureBox2, pictureBox3, pictureBox4, pictureBox5, pictureBox6, pictureBox7, pictureBox8, pictureBox9 };
            boxes = (PictureBox[])box.Clone();
            clear();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listPanel.Add(Menu);
            listPanel.Add(Gra);
            listPanel.Add(panel3);
            listPanel[0].BringToFront();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ai = 1;
            listPanel[1].BringToFront();
            Menu.Hide();
            PictureBox[] box = { pictureBox1, pictureBox2, pictureBox3, pictureBox4, pictureBox5, pictureBox6, pictureBox7, pictureBox8, pictureBox9 };
            boxes = (PictureBox[])box.Clone();
            clear();

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ai = 0;
            listPanel[2].BringToFront();
        }
    }
}
